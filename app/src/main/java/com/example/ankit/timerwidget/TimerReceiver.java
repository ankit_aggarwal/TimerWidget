package com.example.ankit.timerwidget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.widget.RemoteViews;

public class TimerReceiver extends BroadcastReceiver {
    private static boolean isTimerOn = false;
    private static long startTime = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        if (isTimerOn) {
            rv.setChronometer(R.id.chronometer, startTime, null, false);
        } else {
            startTime = SystemClock.elapsedRealtime();
            rv.setChronometer(R.id.chronometer, startTime, null, true);
        }

        isTimerOn = !isTimerOn;

        AppWidgetManager.getInstance(context)
                .updateAppWidget(new ComponentName(context, TimerWidgetProvider.class), rv);
    }
}
